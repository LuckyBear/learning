# encoding:utf-8
"""
    Create on by Alan 2019-10-09 09:43
"""

from flask import Flask
from flask_api import Resource, Api

app = Flask(__name__)
api = Api(app)


class Home(Resource):

    def get(self):
        return {'status': 'success', 'message': 'Flask restful is running'}


api.add_resource(Home, '/')

if __name__ == '__main__':
    app.run(debug=True)
