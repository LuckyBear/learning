# encoding:utf-8
"""
    Create on by Alan 2019-10-09 09:00
"""
from flask import Flask
from time import sleep
from concurrent.futures import ThreadPoolExecutor

# 创建线程池
executor = ThreadPoolExecutor(2)

app = Flask(__name__)


@app.route('/jobs')
def run_jobs():
    executor.submit(long_task, 'hello', 123)
    return 'long task running'


def long_task(arg1, arg2):
    print('args: %s %s!' % arg1, arg2)
    sleep(5)
    print('Task is done!')


if __name__ == '__main__':
   app.run()