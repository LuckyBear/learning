# encoding:utf-8
"""
    Create on by Alan 2019-10-09 15:28
"""

from examples import app
from celery.utils.log import get_task_logger
import time

logger = get_task_logger('__name__')


@app.task(bind=True, queue='attachment_download_pingan_test')
def download(self, **kwargs):
    time.sleep(5)
    logger.info('~~~~download:~~~~~~~')
    return kwargs


@app.task(bind=True, queue='attachment_upload')
def upload(self):
    time.sleep(10)
    logger.info('~~~~upload~~~~~')


@app.task(bind=True, queue='attachment_zip')
def zip(self):
    time.sleep(10)
    logger.info('~~~~zip~~~~~')
