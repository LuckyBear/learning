# encoding:utf-8
"""
    Create on by Alan 2019-10-09 17:38
"""

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.mongodb import MongoDBJobStore
from pymongo import MongoClient
from time import sleep
from config import MONGO_DB, MONGO_HOST, MONGO_PORT


class Scheduler(object):
    mongo = MongoClient(host=MONGO_HOST, port=MONGO_PORT)

    jobstores = {
        'mongo': MongoDBJobStore(database=MONGO_DB,
                                 collection='apscheduler_jobs',
                                 client=mongo)
    }

    scheduler = BackgroundScheduler(jobstores=jobstores)

    def execute_spider(self):
        print('execute_spider')

    def restart(self):
        self.scheduler.shutdown()
        self.scheduler.start()
        print('restarted')

    def update(self):
        print('updating...')

    def run(self):
        print('execute_spider')
        sleep(40)
        self.scheduler.start()


