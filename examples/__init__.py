# encoding:utf-8
"""
    Create on by Alan 2019-10-09 13:39
"""

from celery import Celery

app = Celery('examples')
app.config_from_object('examples.celeryconfig')  # 通过 Celery 实例加载配置模块
