# encoding:utf-8
"""
    Create on by Alan 2019-09-12 14:00
"""
# from datetime import timedelta
# from celery.schedules import crontab

# Broker settings.
BROKER_URL = 'amqp://{}:{}@{}:{}'.format(
    'spider',
    'spider110',
    # '127.0.0.1',
    'be-mq02.wisers.com',
    '5672'
)
# Using the database to store task state and results. 'redis://:password@host:port/db'
CELERY_RESULT_BACKEND = 'redis://{}:{}@{}:{}/{}'.format(
    '',
    '',
    'webcrawl15.wisers.com',
    '6379',
    '8'
)

CELERY_TASK_SERIALIZER = 'msgpack'  # 任务序列化和反序列化使用msgpack方案

CELERY_RESULT_SERIALIZER = 'json'  # 读取任务结果一般性能要求不高，所以使用了可读性更好的JSON

CELERY_TASK_RESULT_EXPIRES = 60 * 60 * 24  # 任务过期时间

CELERY_ACCEPT_CONTENT = ['json', 'msgpack']  # 指定接受的内容类型

CELERY_TIMEZONE = 'Asia/Shanghai'  # 指定时区，默认是 UTC
# CELERY_TIMEZONE='UTC'

CELERY_IMPORTS = (  # 指定导入的任务模块
    'examples.tasks',
)


