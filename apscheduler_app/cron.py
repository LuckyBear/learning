# encoding:utf-8
"""
    Create on by Bear 2019/10/8 22:33
"""
"""
2、cron任务
    表示每天的19：23 分执行任务
    
可以填写数字，也可以填写字符串
    hour =19 , minute =23
    hour ='19', minute ='23'
    minute = '*/3' 表示每 5 分钟执行一次
    hour ='19-21', minute= '23' 表示 19:23、 20:23、 21:23 各执行一次任务
"""


from datetime import datetime
import os
from apscheduler.schedulers.blocking import BlockingScheduler

def tick():
    print('Tick! The time is: %s' % datetime.now())

if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(tick, 'cron', hour=19,minute=23)
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C    '))

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        pass
