# encoding:utf-8
"""
    Create on by Alan 2019-10-09 14:57
"""
# from time import sleep
# from examples.tasks import download
# from celery import group
#
# rounds = 5
# interval = 20
# keyworks = [1, 2, 4, 5, 6]
# priority = 1
#
# for num in range(rounds):
#     for key in keyworks:
#         # 异步发布信息
#         print(key, num)
#         info = {
#             'key': key,
#             'priority': priority
#         }
#         result = download.apply_async(kwargs=info, queue='attachment_download_pingan_test', priority=priority)
#     sleep(interval)
from examples.demo import Scheduler

scheduler = Scheduler()

if __name__ == '__main__':
    scheduler.restart()
